import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda';
import CustomDynamoClient from '../utils/dynamodb';


export const handler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  // All log statements are written to CloudWatch
  console.debug('Received event:', event);

  const dynamoClient = new CustomDynamoClient('teams');
  const teams = await dynamoClient.readAll();
  teams?.entries()

  return {
    statusCode: 200,
    body: JSON.stringify({
      teams: teams,
    })
  };
}

import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda';
import CustomDynamoClient from '../utils/dynamodb';
import * as Parallel from 'async-parallel';
import axios from 'axios';


export const handler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  const dynamoClient = new CustomDynamoClient('teams');
  const teams = (await dynamoClient.readAll())?.map(team => team) ?? [];


  const teamsSlpDiff = await Parallel.map(teams, async (team) => {
    const response = await axios({
      method: 'GET',
      url: `https://game-api.skymavis.com/game-api/clients/0x${team.ronin}/items/1`
    });
    const data: {success: boolean, total: number} = response.data;

    return {
      name: team.name,
      dif: (data.success ? data.total : team.startChallengeSlp) - team.startChallengeSlp
    };
  }, 4);

  console.log('>>>>> teamsSlpDiff', teamsSlpDiff);

  return {
    statusCode: 200,
    body: JSON.stringify({
      teamsSlpDiff: teamsSlpDiff,
    })
  };
}
